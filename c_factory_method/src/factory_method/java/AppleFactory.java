package factory_method.java;

public class AppleFactory implements IFruitFactory {
	@Override
	public IFruit createFruit(boolean mature) {
		return new Apple(mature);
	}
}
