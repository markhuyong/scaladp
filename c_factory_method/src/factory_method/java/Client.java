package factory_method.java;

public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		IFruitFactory factory1 = new AppleFactory();
		IFruit apple1 = factory1.createFruit(true);
		System.out.println(apple1.getTaste());
		IFruit apple2 = factory1.createFruit(false);
		System.out.println(apple2.getTaste());

		IFruitFactory factory2 = new OrangeFactory();
		IFruit orange1 = factory2.createFruit(true);
		System.out.println(orange1.getTaste());
		IFruit orange2 = factory2.createFruit(false);
		System.out.println(orange2.getTaste());
		
	}

}
