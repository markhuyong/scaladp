package singleton.java;

public class LazyThreadSafe {
	
	private static volatile LazyThreadSafe instance;
	
	public static LazyThreadSafe getInstance() {
		if(instance == null){
			synchronized(lazyThreadSafe.class){
				if(instance == null)
					instance = new LazyThreadSafe();
			}
		}  
		return instance;
	}

}
